﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Entity_Framework_Migrations
{
    public class CoachFavoriteSport
    {
        public int CoachId { get; set; }

        public Coach Coach { get; set; }

        public int FavoriteSportsId { get; set; }

        public FavoriteSport favoriteSport { get; set; }


    }
}
