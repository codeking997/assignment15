﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Entity_Framework_Migrations
{
    


    public class Coach
    {
        public int Id { get; set; }

        public string FirstName { get; set; }

        public string LastName { get; set; }

        public string Sport { get; set; }

        //public  int? AthletId { get; set; }

        public ICollection<Athlet> Athlets { get; set; }

        public ICollection<CoachFavoriteSport> coachFavoriteSports { get; set; }



    }
}
