﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Entity_Framework_Migrations
{
    public class FavoriteSport
    {
        public int Id { get; set; }
        public string sport { get; set; }

        public ICollection<CoachFavoriteSport> coachFavoriteSports { get; set; }
    }
}
