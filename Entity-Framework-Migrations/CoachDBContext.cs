﻿using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Text;

namespace Entity_Framework_Migrations
{
    public class CoachDBContext : DbContext
    {
        public DbSet<Coach> Coaches { get; set; }

        public DbSet<Athlet> athlets { get; set; }

        public DbSet<FavoriteSport> favoriteSports{ get; set; }

        public DbSet<CoachFavoriteSport> coachFavoriteSports { get; set; }

        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            optionsBuilder.UseSqlServer("Data Source=PC7585\\SQLEXPRESS;Initial Catalog=CoachDB; Integrated Security=True;");
        }
        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            //modelBuilder.Entity<Coach>().Property(e => e.AthletId).ValueGeneratedNever();
            //modelBuilder.Entity<Coach>().Property(e => e.AthletId).ValueGeneratedNever();
            modelBuilder.Entity<CoachFavoriteSport>()
               .HasKey(bc => new { bc.CoachId, bc.FavoriteSportsId });
            modelBuilder.Entity<CoachFavoriteSport>()
                .HasOne(bc => bc.Coach)
                .WithMany(b => b.coachFavoriteSports)
                .HasForeignKey(bc => bc.CoachId);
            modelBuilder.Entity<CoachFavoriteSport>()
                .HasOne(bc => bc.favoriteSport)
                .WithMany(c => c.coachFavoriteSports)
                .HasForeignKey(bc => bc.FavoriteSportsId);
        
    }
        
    }
}
