﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace Entity_Framework_Migrations.Migrations
{
    public partial class hullo : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_Coaches_athlets_AthletId",
                table: "Coaches");

            migrationBuilder.AlterColumn<int>(
                name: "AthletId",
                table: "Coaches",
                nullable: true,
                oldClrType: typeof(int),
                oldType: "int");

            migrationBuilder.AddForeignKey(
                name: "FK_Coaches_athlets_AthletId",
                table: "Coaches",
                column: "AthletId",
                principalTable: "athlets",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_Coaches_athlets_AthletId",
                table: "Coaches");

            migrationBuilder.AlterColumn<int>(
                name: "AthletId",
                table: "Coaches",
                type: "int",
                nullable: false,
                oldClrType: typeof(int),
                oldNullable: true);

            migrationBuilder.AddForeignKey(
                name: "FK_Coaches_athlets_AthletId",
                table: "Coaches",
                column: "AthletId",
                principalTable: "athlets",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);
        }
    }
}
