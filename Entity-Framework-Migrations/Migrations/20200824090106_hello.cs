﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace Entity_Framework_Migrations.Migrations
{
    public partial class hello : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "CoachId",
                table: "athlets");

            migrationBuilder.AddColumn<int>(
                name: "AthletId",
                table: "Coaches",
                nullable: false,
                defaultValue: 0);

            migrationBuilder.CreateIndex(
                name: "IX_Coaches_AthletId",
                table: "Coaches",
                column: "AthletId");

            migrationBuilder.AddForeignKey(
                name: "FK_Coaches_athlets_AthletId",
                table: "Coaches",
                column: "AthletId",
                principalTable: "athlets",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_Coaches_athlets_AthletId",
                table: "Coaches");

            migrationBuilder.DropIndex(
                name: "IX_Coaches_AthletId",
                table: "Coaches");

            migrationBuilder.DropColumn(
                name: "AthletId",
                table: "Coaches");

            migrationBuilder.AddColumn<int>(
                name: "CoachId",
                table: "athlets",
                type: "int",
                nullable: false,
                defaultValue: 0);
        }
    }
}
