﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace Entity_Framework_Migrations.Migrations
{
    public partial class onetomany : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_Coaches_athlets_AthletId",
                table: "Coaches");

            migrationBuilder.DropIndex(
                name: "IX_Coaches_AthletId",
                table: "Coaches");

            migrationBuilder.DropColumn(
                name: "AthletId",
                table: "Coaches");

            migrationBuilder.AddColumn<int>(
                name: "CoachId",
                table: "athlets",
                nullable: true);

            migrationBuilder.CreateIndex(
                name: "IX_athlets_CoachId",
                table: "athlets",
                column: "CoachId");

            migrationBuilder.AddForeignKey(
                name: "FK_athlets_Coaches_CoachId",
                table: "athlets",
                column: "CoachId",
                principalTable: "Coaches",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_athlets_Coaches_CoachId",
                table: "athlets");

            migrationBuilder.DropIndex(
                name: "IX_athlets_CoachId",
                table: "athlets");

            migrationBuilder.DropColumn(
                name: "CoachId",
                table: "athlets");

            migrationBuilder.AddColumn<int>(
                name: "AthletId",
                table: "Coaches",
                type: "int",
                nullable: true);

            migrationBuilder.CreateIndex(
                name: "IX_Coaches_AthletId",
                table: "Coaches",
                column: "AthletId");

            migrationBuilder.AddForeignKey(
                name: "FK_Coaches_athlets_AthletId",
                table: "Coaches",
                column: "AthletId",
                principalTable: "athlets",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);
        }
    }
}
