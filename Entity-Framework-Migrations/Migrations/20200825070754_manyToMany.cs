﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace Entity_Framework_Migrations.Migrations
{
    public partial class manyToMany : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                name: "favoriteSports",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    sport = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_favoriteSports", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "coachFavoriteSports",
                columns: table => new
                {
                    CoachId = table.Column<int>(nullable: false),
                    FavoriteSportsId = table.Column<int>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_coachFavoriteSports", x => new { x.CoachId, x.FavoriteSportsId });
                    table.ForeignKey(
                        name: "FK_coachFavoriteSports_Coaches_CoachId",
                        column: x => x.CoachId,
                        principalTable: "Coaches",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_coachFavoriteSports_favoriteSports_FavoriteSportsId",
                        column: x => x.FavoriteSportsId,
                        principalTable: "favoriteSports",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateIndex(
                name: "IX_coachFavoriteSports_FavoriteSportsId",
                table: "coachFavoriteSports",
                column: "FavoriteSportsId");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "coachFavoriteSports");

            migrationBuilder.DropTable(
                name: "favoriteSports");
        }
    }
}
