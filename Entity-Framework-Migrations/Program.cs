﻿using Microsoft.EntityFrameworkCore;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;

namespace Entity_Framework_Migrations
{
    class Program
    {
        static void Main(string[] args)
        {
            
            Console.WriteLine("to see the list press d, to remove press r, press a to add" +
                "to update write u, to add a new favorite sport type sport" +
                "to assign a sport to a coach press as, to remove a favorite sport" +
                "write remove, to view the coaches favorite sport press view" +
                "to see as json write json");
            string userInput = Console.ReadLine();
            if (userInput == "d")
            {
                display();
            }else if(userInput == "r")
            {
                RemoveAthlet();
            }else if (userInput == "a")
            {
                addCoach();
            }else if (userInput == "u")
            {
                UpdateAthlet();
            }else if (userInput == "sport")
            {
                addFavSport();
            }else if (userInput == "as")
            {
                assignFavSportToCoach();
            }else if(userInput == "remove")
            {
                removeFavSport();
            }else if(userInput == "view") {
                DisplayFavSport();
            }else if (userInput == "json")
            {
                JsonTaskMethod();
            }

            
        }
        private static void JsonTaskMethod()
        {
            using (CoachDBContext coachDBContext = new CoachDBContext()) {
                List<Athlet> athlets = coachDBContext.athlets
                    .Include(s => s.Coach)
                    .ToList();
                string jsonAthlet = JsonConvert.SerializeObject(athlets, Formatting.Indented, 
                    new JsonSerializerSettings() { ReferenceLoopHandling = ReferenceLoopHandling.Ignore});
                Console.WriteLine(jsonAthlet);
                    
                                                     
            }
        }
        /*
         * this method allows the user to create a new coach and a athlet as he pleases
         * it creates a connection then it uses user input to ask the user to provide detalis about
         * the new coach or athlet. when finished it saves the changes
         */
        private static void addCoach()
        {
            using (CoachDBContext coachDBContext = new CoachDBContext())
            {
                Console.WriteLine("write c for coach and a for athlet");
                string coachInput = Console.ReadLine();
                if (coachInput == "c")
                {
                    Console.WriteLine("enter a first name");
                    string firstName = Console.ReadLine();
                    Console.WriteLine("enter a last name");
                    string lastName = Console.ReadLine();
                    Console.WriteLine("enter a sport");
                    string sport = Console.ReadLine();
                    coachDBContext.Coaches.Add(new Coach { FirstName = firstName, LastName = lastName, Sport = sport });
                }
                else if (coachInput == "a")
                {
                    Console.WriteLine("enter a first name");
                    string firstName = Console.ReadLine();
                    Console.WriteLine("enter a last name");
                    string lastName = Console.ReadLine();
                    Console.WriteLine("enter a age");
                    int ageOfAthlet = Convert.ToInt32(Console.ReadLine());
                    Console.WriteLine("enter an id for coach");
                    int idOfCoach = Convert.ToInt32(Console.ReadLine());
                    coachDBContext.athlets.Add(new Athlet { FirstName = firstName, LastName = lastName, age = ageOfAthlet, CoachId=idOfCoach});
                }
                coachDBContext.SaveChanges();
                /*var coach = coachDBContext.Coaches.Include(s => s.athlet).SingleOrDefault(s => s.Id == 1);
                Console.WriteLine($"the name of the coach is {coach.FirstName} and he trains {coach.athlet.LastName}");*/
                

            }
        }
        /*
         * this method allows the user to display all the coaches and who they train
         * establishes a connection to the database
         * uses a foreach to go through the list of coaches. 
         * it then prints out the names of coaches
         * the if statement checks if there is a relation between athlet and coach
         * if not it will not run, if so it will show the different coaches and who they train.
         */
        private static void display()
        {
            using (CoachDBContext coachDBContext = new CoachDBContext())
            {
                var coaches = coachDBContext.Coaches.Select(s => s).Include(p => p.Athlets);
                foreach (Coach coach in coaches) {
                   
                    Console.WriteLine($"{coach.FirstName} {coach.LastName}");

                    if (coach.Athlets != null)
                    {
                        foreach (Athlet athlets in coach.Athlets)
                        {
                            Console.WriteLine($"{athlets.FirstName} {athlets.LastName}");
                            Console.WriteLine($"the name of the coach is {coach.FirstName} {coach.LastName} and he coaches {athlets.FirstName} {athlets.LastName}");
                        }
                    }
                    
                }
                
            }
        }
        private static void DisplayFavSport()
        {
            using (CoachDBContext coachDBContext = new CoachDBContext())
            {
                int userInput = Convert.ToInt32(Console.ReadLine());
                var coachFavsport = coachDBContext.coachFavoriteSports.Select(s => s).Where(s => s.CoachId == userInput);
                foreach(CoachFavoriteSport coachFavoriteSport in coachFavsport)
                {
                    Console.WriteLine(coachFavsport);
                }

            }
        }
        /*
         * this method allows the user to change details about the athlet
         * establishes connection to the database
         * gives instructions, then goes through the different options
         * the ifs and elses are the different options to change
         * one can change, firstname, lastname, age and the coach by his coachId
         */
        private static void UpdateAthlet()
        {
            using (CoachDBContext coachDBContext = new CoachDBContext())
            {
                Console.WriteLine("write id of athlet you want to update: ");
                int athletChangeId = Convert.ToInt32(Console.ReadLine());
                var athlet = coachDBContext.athlets.Find(athletChangeId);
                Console.WriteLine("do you want to change the first name press f, " +
                    "the last name then press l, or the age then press a?" +
                    "to change coach press c");
                string userChangeInput = Console.ReadLine();
                coachDBContext.SaveChanges();
                if(userChangeInput == "f")
                {
                    Console.WriteLine("write the new name: ");
                    string firstNameChange = Console.ReadLine();
                    athlet.FirstName = firstNameChange;
                    Console.WriteLine("the new first name is: " + firstNameChange);
                    coachDBContext.SaveChanges();
                }else if(userChangeInput == "l")
                {
                    Console.WriteLine("write the new last name: ");
                    string lastNameChange = Console.ReadLine();
                    athlet.LastName = lastNameChange;
                    Console.WriteLine("the new last name is: " + lastNameChange);
                    coachDBContext.SaveChanges();
                }else if (userChangeInput == "a")
                {
                    Console.WriteLine("write the new age: ");
                    int ageChange = Convert.ToInt32(Console.ReadLine());
                    athlet.age = ageChange;
                    Console.WriteLine("the new age is: " + ageChange);
                    coachDBContext.SaveChanges();
                }else if (userChangeInput == "c")
                {
                    Console.WriteLine("write the id of the new coach: ");
                    int coachIdChange =Convert.ToInt32(Console.ReadLine());
                    athlet.CoachId = coachIdChange;
                    Console.WriteLine("the new coach id is is: " + coachIdChange);
                    coachDBContext.SaveChanges();
                }
            }
        }
        /*
         * this method allows the user to remove an athlet
         * works the same as the above mentioned methods.
         */
        private static void RemoveAthlet()
        {
            using (CoachDBContext coachDBContext = new CoachDBContext())
            {
                Console.WriteLine("enter c to remove Coach, a to remove athlet");
                string userRemoveInput = Console.ReadLine();
                if (userRemoveInput == "c")
                {
                    Console.WriteLine("enter a number to remove");
                    int removeCoachId = Convert.ToInt32(Console.ReadLine());
                    var coach = coachDBContext.athlets.Find(removeCoachId);
                    
                    coachDBContext.athlets.Remove(coach);
                    Console.WriteLine($"coach was removed: {coach.FirstName} {coach.LastName}");
                    coachDBContext.SaveChanges();
                }
                else if (userRemoveInput == "a")
                {
                    Console.WriteLine("enter a number to remove");
                    int removeId = Convert.ToInt32(Console.ReadLine());
                    var athlet = coachDBContext.athlets.Find(removeId);
                    //var sportsmen = coachDBContext.athlets.Where(s => s.);
                    coachDBContext.athlets.Remove(athlet);
                    Console.WriteLine($"athlet was removed: {athlet.FirstName} {athlet.LastName}");
                    coachDBContext.SaveChanges();
                }
            }
        }/*
          * this method allows the user to add a new sport in the sports table
          */
        private static void addFavSport()
        {
            using (CoachDBContext coachDBContext = new CoachDBContext())
            {
                Console.WriteLine("type in the new sport");

                string favSport = Console.ReadLine();
                coachDBContext.favoriteSports.Add(new FavoriteSport { sport=favSport });
                Console.WriteLine("the new sport is: " + favSport);
                coachDBContext.SaveChanges();
            }
        }
        /*
         * this method allows the user to assign a fav sport to a coach
         * 
         */
        private static void assignFavSportToCoach()
        {
            using (CoachDBContext coachDBContext = new CoachDBContext())
            {
                Console.WriteLine("write the Id of the coach you want to update his favorite sport");
                int coachAddsportInt = Convert.ToInt32(Console.ReadLine());
                Console.WriteLine("add sport id");
                int favSportInt = Convert.ToInt32(Console.ReadLine());
                //var coach = coachDBContext.coachFavoriteSports.Select(s=> s ).Where(s=> s.CoachId==coachAddsportInt);
                CoachFavoriteSport coachFavoriteSport = new CoachFavoriteSport();
                coachFavoriteSport.CoachId = coachAddsportInt;
                coachFavoriteSport.FavoriteSportsId = favSportInt;
                coachDBContext.coachFavoriteSports.Add(coachFavoriteSport);
                coachDBContext.SaveChanges();
            }
        }
        /*
         * this method removes a sport from the list of favorite coaches of sports. 
         */
        private static void removeFavSport()
        {
            using (CoachDBContext coachDBContext = new CoachDBContext())
            {
                Console.WriteLine("write id of coach who you want to remove the fav sport from");
                int coachRemoveSportInt = Convert.ToInt32(Console.ReadLine());
                Console.WriteLine("enter sport id to remove");
                int removeSportInt = Convert.ToInt32(Console.ReadLine());
                CoachFavoriteSport coachFavoriteSport = new CoachFavoriteSport();
                coachFavoriteSport.CoachId = coachRemoveSportInt;
                coachFavoriteSport.FavoriteSportsId = removeSportInt;
                coachDBContext.coachFavoriteSports.Remove(coachFavoriteSport);
                coachDBContext.SaveChanges();
                Console.WriteLine("sport was removed: " + coachFavoriteSport);
            }

        }
        
    }
}
